package aids61517.gophergamekotlin

import aids61517.gophergamekotlin.data.GeneralGopher
import aids61517.gophergamekotlin.data.SpecialGopher
import aids61517.gophergamekotlin.databinding.MainActivityBinding
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View

class MainActivityActivity : AppCompatActivity() {
    private lateinit var mBinding: MainActivityBinding
    private val mViewModel by lazy { ViewModelProviders.of(this).get(MainViewModel::class.java) }
    private val mHoleViewArray by lazy {
        arrayOf(mBinding.hole1, mBinding.hole2, mBinding.hole3
                , mBinding.hole4, mBinding.hole5, mBinding.hole6
                , mBinding.hole7, mBinding.hole8, mBinding.hole9)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = DataBindingUtil.setContentView(this, R.layout.main_activity)
        mBinding.viewModel = mViewModel
        mBinding.setLifecycleOwner(this)
        lifecycle.addObserver(mViewModel)

        initView()
    }

    private fun initView() {
        val gopherList = mViewModel.gopherLiveDataList
        for ((index, holeView) in mHoleViewArray.withIndex()) {
            gopherList[index].observe(this, Observer {
                holeView.tag = it
                if (it == null) {
                    holeView.setImageResource(0)
                    return@Observer
                }

                when (it) {
                    is GeneralGopher -> holeView.setImageResource(R.drawable.general_gopher)
                    is SpecialGopher -> holeView.setImageResource(R.drawable.special_gopher)
                    else -> holeView.setImageResource(R.drawable.bad_gopher)
                }
            })
        }

        mViewModel.gameState.observe(this, Observer {
            val gameState = it!!
            when (gameState) {
                GameState.PLAYING -> {
                    mBinding.pauseButton.setText(R.string.text_pause)
                    mBinding.pauseButton.visibility = View.VISIBLE
                }
                GameState.STOP -> {
                    mBinding.pauseButton.visibility = View.GONE
                }
                GameState.PAUSE -> {
                    mBinding.pauseButton.setText(R.string.text_resume)
                    mBinding.pauseButton.visibility = View.VISIBLE
                }
            }
        })
    }
}
