package aids61517.gophergamekotlin

import aids61517.gophergamekotlin.data.*
import android.arch.lifecycle.*
import android.util.Log
import android.view.View
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.properties.Delegates

enum class GameState {
    PLAYING, STOP, PAUSE
}

class MainViewModel : ViewModel(), LifecycleObserver {
    val gameState = MutableLiveData<GameState>().apply { value = GameState.STOP }
    val gameScore = MutableLiveData<String>().apply { value = "0" }
    val gopherLiveDataList = mutableListOf<MutableLiveData<Gopher>>().apply {
        for (i in 0 until 9) {
            add(MutableLiveData())
        }
    }
    private var mScore by Delegates.observable(0) { _, _, newValue -> gameScore.value = newValue.toString() }
    private var mCompositeDisposable: CompositeDisposable = CompositeDisposable()
    private val mGopherDisposableMap = mutableMapOf<Gopher, Disposable>()
    private val mRandom = Random()
    private val mGopherList = mutableListOf<Gopher>()

    companion object {
        const val SCORE_NON_GOPHER = -100
    }

    fun clickGameStartButton() {
        val state = gameState.value!!
        when (state) {
            GameState.PLAYING, GameState.PAUSE -> {
                stopGame()
            }
            else -> {
                startGame()
            }
        }
    }

    fun clickGamePauseButton() {
        val state = gameState.value!!
        when (state) {
            GameState.PLAYING -> {
                pauseGame()
            }
            GameState.PAUSE -> {
                resumeGame()
            }
        }
    }

    private fun pauseGame() {
        gameState.value = GameState.PAUSE
        for (gopher in mGopherList) {
            val currentTime = Calendar.getInstance().timeInMillis
            val passedTime = currentTime - gopher.lastUpdateTime
            if (gopher.state == Gopher.State.HIDING) {
                gopher.waitingTime -= passedTime
            } else {
                gopher.appearTime -= passedTime
            }
        }

        mCompositeDisposable.dispose()
    }

    private fun resumeGame() {
        gameState.value = GameState.PLAYING
        mCompositeDisposable = CompositeDisposable()
        for (gopher in mGopherList) {
            gopher.lastUpdateTime = Calendar.getInstance().timeInMillis
            if (gopher.state == Gopher.State.HIDING) {
                registerTimer(gopher, gopher.waitingTime, mShowGopherAction)
            } else {
                registerTimer(gopher, gopher.appearTime, mCreateGopherAction)
            }
        }
    }

    private fun startGame() {
        Log.d("123", "startGame")
        gameState.value = GameState.PLAYING
        mScore = 0
        mCompositeDisposable = CompositeDisposable()
        for (i in 0 until gopherLiveDataList.size) {
            createGopher(i)
        }
    }

    private fun createGopher(position: Int) {
        val random = mRandom.nextInt(100)
        val gopher = GopherFactor.create(random)
        if (mGopherList.size <= position) {
            mGopherList.add(gopher)
        } else {
            mGopherList[position] = gopher
        }

        registerTimer(gopher, gopher.waitingTime, mShowGopherAction)
    }

    private fun registerTimer(gopher: Gopher, time: Long, action: Gopher.() -> Unit) {
        Observable.timer(time, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    mGopherDisposableMap[gopher]?.apply {
                        mGopherDisposableMap.remove(gopher)
                    }

                    gopher.action()
                }.also { mCompositeDisposable.add(it) }
                .also { mGopherDisposableMap[gopher] = it }
    }

    private fun stopGame() {
        gameState.value = GameState.STOP
        mCompositeDisposable.dispose()
        mGopherDisposableMap.clear()
        gopherLiveDataList.forEach { it.value = null }
        mGopherList.clear()
    }

    fun clickHole(view: View) {
        if (gameState.value!! != GameState.PLAYING) {
            return
        }

        val gopher = view.tag as? Gopher
        gopher?.also {
            val disposable = mGopherDisposableMap.remove(gopher)
            disposable?.apply { mCompositeDisposable.remove(this) }

            val position = mGopherList.indexOf(gopher)
            gopherLiveDataList[position].value = null
            createGopher(position)
        }

        calculateScore(gopher?.score ?: SCORE_NON_GOPHER)
    }

    private fun calculateScore(score: Int) {
        mScore = if (mScore + score < 0) 0 else mScore + score
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    fun onStop() {
        Log.d("123", "onStop")
        if (gameState.value!! == GameState.PLAYING) {
            pauseGame()
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    fun onDestroy() {
        Log.d("123", "onDestroy")
        mCompositeDisposable.dispose()
    }

    private val mShowGopherAction: Gopher.() -> Unit = {
        val position = mGopherList.indexOf(this)
        gopherLiveDataList[position].value = this
        state = Gopher.State.APPEAR
        lastUpdateTime = Calendar.getInstance().timeInMillis
        registerTimer(this, appearTime, mCreateGopherAction)
    }

    private val mCreateGopherAction: Gopher.() -> Unit = {
        val position = mGopherList.indexOf(this)
        gopherLiveDataList[position].value = null
        createGopher(position)
    }
}