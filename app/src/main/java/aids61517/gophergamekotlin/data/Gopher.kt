package aids61517.gophergamekotlin.data

import java.util.*

abstract class Gopher {
    abstract val score: Int
    var waitingTime = createRandomTime(WAITING_TIME_MIN, WAITING_TIME_MAX)
    var appearTime = createRandomTime(APPEAR_TIME_MIN, APPEAR_TIME_MAX)
    var lastUpdateTime = Calendar.getInstance().timeInMillis
    var state = State.HIDING

    companion object {
        const val WAITING_TIME_MIN = 800
        const val WAITING_TIME_MAX = 5000
        const val APPEAR_TIME_MIN = 3200
        const val APPEAR_TIME_MAX = 6000
    }

    protected fun createRandomTime(minTime: Int, maxTime: Int): Long {
        return (Random().nextInt(maxTime - minTime + 1) + minTime).toLong()
    }

    enum class State {
        HIDING, APPEAR
    }
}