package aids61517.gophergamekotlin.data

class GopherFactor {
    companion object {
        fun create(randomValue: Int) = when (randomValue) {
            in 0..59 -> GeneralGopher()
            in 60..79 -> SpecialGopher()
            else -> BadGopher()
        }
    }
}